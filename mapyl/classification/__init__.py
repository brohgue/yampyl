from .softmax_class import SoftMaxClasser
from .svm import SVM
from ._knear import KNearestNeighbors
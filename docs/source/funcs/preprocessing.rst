Preprocessing
=============

Module for preprocessing data

.. currentmodule:: mapyl.preprocessing

.. autoclass:: Normalizer
    :members:

.. autoclass:: MinMaxScaler
    :members:

.. autoclass:: MeanNormalizer
    :members:
   
.. autoclass:: StandardScaler
    :members:

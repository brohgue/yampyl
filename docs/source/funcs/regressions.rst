==========
Regression
==========

Module for regression tasks.
Regression tasks aim to obtain the equation for a set of data and use that equation
in order to predict values.

.. currentmodule:: mapyl.regression

.. autoclass:: LinearRegressor
    :members:

.. autoclass:: GradientDescent
    :members:

.. autoclass:: SGD
    :members:

.. autoclass:: PolyRegressor
    :members:

.. warning:: The PolyRegressor instance does a polynomial expansion in order to work, 
        so if there are too many degrees or too many features you can have overflow

.. autoclass:: BinLogitRegressor
    :members:

.. note:: The float returned by predict_prob represents the probability of 
    the supplied instance being 1 or 0, a probability larger than 0.5 means that it belongs to 1
    and a probability less than 0.5 means that it belongs to 0.         
===============================
Fully Connected Neural Networks
===============================

Fully Conected Neural Networks module.

Contains Dense and Dropout layers along with ReLU and SoftMax activation functions.

This module is based on the syntax that Tensorflow/Keras uses.

.. currentmodule:: mapyl.NN

.. code-block:: python

    from NN import Model
    from NN.layers import Dense, Dropout
    from NN.activations import ReLU, SoftMax
    from NN.loss import CatCrossEntr
    from NN.optimizers import SGDOptimizer

    # the X and y datasets
    X = a_dataset
    y = another_dataset

    # create loss and optimizer objects
    loss = CatCrossEntr()
    optimizer = SGDOptimizer(lr=0.1, decay=1E-3, momentum=0.6)

    # create Model object
    model = Model()
    model.add(Dense(2, 32))
    model.add(ReLU())
    model.add(Dropout(rate=0.2))
    model.add(Dense(32, 3))
    model.add(SoftMax())

    # finalize the model
    model.finalize(loss=loss, optimizer=optimizer)

    # train the model
    model.fit(X, y, epochs=300,validation_data=None, verbose=2)


The Model object
^^^^^^^^^^^^^^^^

.. autoclass:: Model
    :members:

The Layer objects
^^^^^^^^^^^^^^^^^

.. currentmodule:: mapyl.NN.layers

.. autoclass:: Dense
    :members:

.. autoclass:: Dropout
    :members:


The Optimizer objects
^^^^^^^^^^^^^^^^^^^^^

.. currentmodule:: mapyl.NN.optimizers

.. autoclass:: SGDOptimizer
    :members:

The Loss objects
^^^^^^^^^^^^^^^^

.. currentmodule:: mapyl.NN.loss

.. autoclass:: CatCrossEntr
    :members:

The Activation Function objects
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. currentmodule:: mapyl.NN.activations

.. autoclass:: ReLU
    :members:

.. autoclass:: SoftMax
    :members:
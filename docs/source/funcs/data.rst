Data
====

module for handling and generating data

.. currentmodule:: mapyl.data

.. autoclass:: GenData
    :members:

.. autoclass:: SplitData
    :members:
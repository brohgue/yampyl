==============
Classification
==============

Module for classification tasks.
The classes in this module accept a ndarray of the values to be inputted (X) and a ndarray 
of the classes of the output. Their predict methods output the class of the supplied values.

.. currentmodule:: mapyl.classification


.. autoclass:: SoftMaxClasser
    :members:

Usage:

    .. code-block:: python

        >>> X = np.array([[4, -3], 
                            [9, -6],
                            [3, -5],
                            [4, -3],
                            [9, -5,]])
        >>> y = np.array([[0],
                            [1],
                            [0],
                            [0],
                            [1]])

        >>> SMC = SoftMaxClasser(2)
        >>> SMC.fit(X, y)
        >>> print(SMC.predict(np.array([[9, -7]])))
        >>> [1]
    

.. autoclass:: SVM
    :members:

.. warning:: The SVM instance does a polynomial expansion in order to work when the degree is over 1, 
        so if there are too many degrees or too many features you can have overflow 
    
Usage:
    .. code-block:: python

        >>> X = np.array([[4, -3], 
                          [9, -6],
                          [3, -5],
                          [4, -3],
                          [9, -5,]])
        >>> y = np.array([[0],
                          [1],
                          [0],
                          [0],
                          [1]])

        >>> SVM_ = SVM(lr=0.01, lam=0.01, iters=300, degree=1)
        >>> SVM_.fit(X, y)
        >>> print(SVM_.predict(np.array([[9, -7]])))
        >>> [1]

.. autoclass:: KNearestNeighbors
    :members:
    
Usage:
    .. code-block:: python

        >>> X = np.array([[4, -3], 
                          [9, -6],
                          [3, -5],
                          [4, -3],
                          [9, -5,]])
        >>> y = np.array([[0],
                          [1],
                          [0],
                          [0],
                          [1]])

        >>> KNN = KNearestNeighbors(K=3)
        >>> KNN.fit(X, y)
        >>> print(KNN.predict(np.array([[9, -7]])))
        >>> [1]